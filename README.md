Чтобы читать мануал на **русском** языке, просто **проскрольте** страницу примерно до середины ;)  


Simple Website Framework
========================

You can use this framework to create simple websites for internal purposes, study projects and
other non-complicated stuff.
Here are a couple examples:  

* [Congrats.Cf](http://congrats.cf)  
* [Exams.Gq](http://exams.gq)  


Structure of the framework
--------------------

By default a project contains the following folders:  

* `css` for styles;  
* `js` for front-end scripts;  
* `php-classes` for PHP back-end;  
* `templates` for templates, that consist of a mix of HTML mark-up and PHP code.  

File `index.php` is used for:  

* initialization of class autoloading;  
* launching of the mechanisms of the framework;  
* switching between normal, debug and maintenance modes (note that you must have write some code in
your controllers to make this work).  
  
If a page cannot be found, the user will be redirected to `404.html`. If the maintenance mode is
enabled the framework will show `maintenance.html` to him. You're able to change the standard behavior
in `ControllerManager.php`.  


Dependencies
------------

### Composer

For a beautiful appearance and the correct work of modal windows in `common.js` the framework uses
**Bootstrap**. In its turn, it depends on **jQuery**.  

### Bower

For word processing and secure storage and output of the user input, I suggest you use
[KozMUL](http://nekochan.ru/KozMUL/manual/) — a special mark-up language, developed by me,
which you can transform to usual HTML code using a JavaScript library. In the future I'm going to
write a back-end processor in PHP. KozMUL, in its turn, depends on a small set of some useful
functions named [KozUtils](http://kozalo.ru/#post-1482074368)
[[documentation in English](https://bitbucket.org/Kozalo/kozutils-js)].  


### npm

The framework uses **Gulp** as a building system. The following modules are used: *cssmin*, *rename*,
*util*, *uglify*, *uglify-js-harmony*.  


Controllers
-----------

*ControllerManager* loads and creates instances of all classes that implement
*IController* and are listed in `list_of_controllers.cfg`.  
The interface, strictly speaking, requires implementations of only two methods:  

* `GetPathPatterns()` — must return an array of strings to let *ControllerManager* match URL
with them. For patterns use a usual for PHP syntax of regular expressions. Omit the slash at the
beginning of strings (except the root: use "/" for it).  
* `ProcessRequest($matchedPattern, $urlPath)` — this method will be called if the URL matches one
of the patterns described in the method above, and it's responsible for processing the request and
displaying a web page. As the first parameter, it takes the matched template. As the second one,
it takes the URL path without website address. Use the first parameter if you're using simple
patterns without any regular expressions, and the second one otherwise.  

See `TestController.php` as an example of a ready-to-use controller. Probably, you'll understand the
basic concept from there:

1. list all sections that are used by the page;  
2. list all necessary scripts and styles;  
3. fill out the array with all required data;  
4. import the basic template, which, in its turn, will import the others.  

Anyway, no one forbid you to rewrite the workflow completely.  


Templates
---------

The design of the website is described with templates consisting of HTML mark-up and a bit of PHP
code to make the site dynamic. According my proposed structure, there is a basic template in file
`basement`. This file contains the root HTML block and includes scripts and styles. The second goal
of the basement is to include templates `header` and `body`. The former is responsible, obviously,
for the header of the website. The latter includes all sections described in a controller.  
See `version_section` as a sample of a section.


Front-end
--------------------------------------

All JavaScript code that is common for all pages of the website is contained by the file `common.js`.
You should also write such common code there (there is a special place for it). Styles are treated
similarly.  
Styles and scripts specified for only particular pages is better to write in separate files and
include them via the special list in a controller.    

For your convenience I wrote two useful functions in `common.js` to let you show some messages to
the user on modal windows:

* `ShowErrorModal(text)` — shows a modal window to notify the user about some error;  
* `ShowSuccessModal(text)` — shows a modal window to notify the user about success.  

There are two utility functions as well:  

* `ProcessText(text)` — adapter that wraps KozMUL processor to let you change its
parameters globally in one place. By default, this method is called for all elements with class
`.section-text`.  
* `PerformPostRequest(url, data, statusField, successCallback, failMessage, clickedButton)` —
Wrapper over *jQuery.post()*, which checks the correctness and success of a response automatically.
In the case of success, it calls **successCallback** which may take the data as a parsed object.
Otherwise, the function shows the modal window with **failMessage**. Use this function if:  
    * you get responses in JSON;  
    * they have a boolean field which indicates the success of a request;  
    * _[optional]_ The trigger of the request is a button with `data-loading-text` (see
    Bootstrap documentation).  


Building
--------

**Gulp** is responsible for building a complete, ready-to-upload version of the website. There are
several built-in tasks for that:  

* `build-css` / `build-js` — minifies styles (into `css/dist`) and scripts (`js/dist`)
respectively;  
* `build` — runs the tasks above simultaneously;  
* `make-dist` — builds the project into `_builds/<build_id>`.  

Feel free to modify these tasks in `gulpfile.js` if you need to.  


*© Leonid Kozarin, 2017*

--------------------------------

Фреймворк для простого веб-сайта
================================

С помощью этого фреймворка Вы можете создавать всякие простые сайты для служебных
целей, учебных проектов и всякой такой мелочёвки.  
Примеры сайтов, построенных на нём:  

* [Congrats.Cf](http://congrats.cf)  
* [Exams.Gq](http://exams.gq)  


Структура фреймворка
--------------------

По умолчанию проект состоит из следующих папок:  

* `css` для стилей;  
* `js` для фронтенд-скриптов;  
* `php-classes` для PHP-бэкенда;  
* `templates` для шаблонов из смеси HTML-разметки и PHP-кода.  

Файл `index.php` используется только для инициализации автозагрузки классов, запуска
самого фреймворка и переключения режимов (есть *debug* и *maintenance*). Правда, за
поддержку этих режимов отвечают написанные Вами контроллеры.  
Для несуществующих страниц отображается файл `404.html`, а при включённом режиме
обслуживания сайта — `maintenance.html`. Стандартное поведение можно изменить в
`ControllerManager.php`.  


Зависимости
-----------

### Composer

Для красивого оформления и работы встроенных в `common.js` модальных окон используется
**Bootstrap**. За собой он тянет широко используемый **jQuery**.  

### Bower

Для оформления текстов и безопасного хранения и отображения пользовательского ввода я
предлагаю использовать [KozMUL](http://nekochan.ru/KozMUL/manual/ru/) — специальный
язык разметки, разработанный мною, который с помощью JavaScript-библиотеки преобразуется
в HTML-код. В будущем планируется возможность "компиляции" текстов на стороне сервера.  
KozMUL же в свою очередь тянет небольшой пакет функций-утилит
[KozUtils](http://kozalo.ru/#post-1482074368).  

### npm

Для сборки готовых билдов используется **Gulp** c рядом библиотек: *cssmin*, *rename*,
*util*, *uglify*, *uglify-js-harmony*.  


Контроллеры
-----------

*ControllerManager* подгружает и создаёт экземпляры всех классов, реализующих интерфейс
*IController* и перечисленных в файле `list_of_controllers.cfg`.  
Интерфейс, собственно, требует реализации всего двух методов:  

* `GetPathPatterns()` — должен вернуть массив из строк-шаблонов для URL, обработку
которых берёт на себя контроллер. Для сопоставления используется обычный синтаксис
регулярных выражений в PHP, а начальный слэш упускается (кроме корневого адреса сайта).  
* `ProcessRequest($matchedPattern, $urlPath)` — этот метод вызывается, если URL совпадает
с одним из указанных в предыдущем методе шаблонов, и отвечает за обработку запроса и/или
отображение веб-страницы. Первым параметром передаётся шаблон, который совпал при сопоставлении.
Вторым параметром передаётся полный путь URL от корня сайта (за исключением самого адреса сайта).
Для сопоставления строк удобно использовать первый параметр, а при использовании регулярных
выражений следует пользоваться вторым.  

В качестве примера контроллера имеется `TestController.php`. В нём показана общая суть:

1. перечисляем список секций на странице;  
2. перечисляем список необходимых странице файлов стилей и скриптов;  
3. заполняем массив остальными необходимыми данными;  
4. импортируем базовый шаблон, который, в свою очередь, импортирует все остальные.  

Впрочем, никто не запрещает Вам полностью поменять мою схему работы.  


Шаблоны
-------

Всё оформление сайта в виде HTML-разметки описывается шаблонами, содержащими вставки PHP-кода
для добавления необходимых динамических данных.  
Согласно предлагаемой структуре существует базовый шаблон `basement`, который содержит главный
HTML-блок, подключения всех стилей и скриптов (для этого он пользуется переданными из контроллера
списками), а также подключает шаблоны `header` и `body`, отвечающие за заголовок и основное
содержимое сайта соответственно. В `body` же находится код, подключающий перечисленные в списке
из контроллера секции. В качестве примера секции можете рассмотреть файл `version_section`.  


Фронтенд
--------------------------------------

Весь общий JavaScript-код для всех разделов сайта стоит дописывать в указанное для этого место
в `common.js`. Аналогично дело обстоит и со стилями. Скрипты и стили, специфичные для конкретных
страниц лучше писать в отдельных файлах, подключаемых через списки в контроллере.  

Для удобства в `common.js` по умолчанию прописаны две функции, которые можно использовать
для вывода информационных модальных окон:  

* `ShowErrorModal(text)` — выводит модальное окно для оповещения об ошибке с указанным сообщением;  
* `ShowSuccessModal(text)` — выводит модальное окно для оповещения об успешности операции.  

Также есть две утилитные функции:  

* `ProcessText(text)` — функция-адаптер, оборачивающая вызов KozMUL-процессора, чтобы можно было
глобально менять его параметры в одном месте. По умолчанию этот метод вызывается для всех
элементов с классом `.section-text`.  
* `PerformPostRequest(url, data, statusField, successCallback, failMessage, clickedButton)` —
дополнительная абстракция над *jQuery.post()*, автоматически проверяющая корректность ответа и
успешность запроса. В случае успеха вызывается **successCallback**, которому передаются уже
распарсенные данные в виде объекта, а при ошибке выводится модальное окно с сообщением
**failMessage**. Используйте эту функцию, если:  
    * вы получаете ответы от сервера в формате JSON;  
    * в них есть булево поле, сообщающее об успешности операции;  
    * _[необязательно]_ запрос активирует кнопка, имеющая Bootstrap-свойство `data-loading-text`.  


Сборка
------

Сборку готового сайта, предназначенного для загрузки на сервер, осуществляет **Gulp**. Для него
определены следующие задачи:  

* `build-css` / `build-js` — осуществляют минификацию стилей (в `css/dist`) и скриптов
(`js/dist`) соответственно;  
* `build` — выполняет две перечисленные выше задачи одновременно;  
* `make-dist` — осуществляет окончательную сборку проекта в `_builds/<build_id>`.  

Смело редактируйте эти задачи в `gulpfile.js` при необходимости.  


*© Леонид Козарин, 2017*
