<?php
use ru\kozalo\simplewebsiteframework\controllers\ControllerManager;

# Autoloading of classes
set_include_path(get_include_path() . PATH_SEPARATOR . "php-classes");
spl_autoload_extensions(".php");
spl_autoload_register();

require("vendor/autoload.php");


# Comment this string on production to decrease verbosity of the code.
ControllerManager::DebugMode();
# Uncomment this string on production to temporary disable the website.
#ControllerManager::MaintenanceMode();

ControllerManager
    ::Init()    # Here you may tell ControllerManager that you want to use your own namespace for controllers.
    ->InvokeAppropriateController();
