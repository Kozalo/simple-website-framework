var gulp = require('gulp');
var cssmin = require('gulp-cssmin');
var minifier = require('gulp-uglify/minifier');
var uglifyjs = require('uglify-js-harmony');
var gutil = require('gulp-util');
var rename = require('gulp-rename');

var currentDate = new Date();

gulp.task('default', function() {
    console.log("You can type 'gulp <taskname>' to use automatic build system.");
    console.log("There are some following tasks:");
    console.log("* build-css -- Minifies all CSS files in the /css directory and places the output files into the /css/dist directory.");
    console.log("* build-js -- Minifies all JS files in the /js directory and places the output files into the /js/dist directory.");
    console.log("* build -- Does both 'build-css' and 'build-js' successively.");
    console.log("* make-dist -- Creates the final build of the website and places it into a /_builds/%buildNumber% directory.");
});


gulp.task('build-css', function() {
    return gulp.src('./css/*.css')
        .pipe(cssmin()).on('error', gutil.log)
        .pipe(gulp.dest('./css/dist'));
});

gulp.task('build-js', function() {
    return gulp.src('./js/*.js')
        .pipe(minifier({compress: false}, uglifyjs)).on('error', gutil.log)
        .pipe(gulp.dest('./js/dist'));
});

gulp.task('build', ['build-css', 'build-js']);

gulp.task('make-dist', ['build'], function() {
    var dateStr = currentDate.getDate().toString() + currentDate.getMonth().toString() + currentDate.getYear().toString();
    var timeStr = currentDate.getHours().toString() + currentDate.getMinutes().toString() + currentDate.getSeconds().toString();
    var buildNumber = dateStr + '-' + timeStr;
    var buildPath = './_builds/' + buildNumber;

    gulp.src('./css/dist/*.css').pipe(gulp.dest(buildPath + '/css'));
    gulp.src('./js/dist/*.js').pipe(gulp.dest(buildPath + '/js'));

    gulp.src('./images/backgrounds/*.{jpg,png}').pipe(gulp.dest(buildPath + '/images/backgrounds'));
    gulp.src('./images/backgrounds/default/*.{jpeg,png}').pipe(gulp.dest(buildPath + '/images/backgrounds/default'));
    gulp.src('./images/logo*.png').pipe(gulp.dest(buildPath + '/images'));

    gulp.src(['.htaccess', '404.html', 'maintenance.html', 'index.php']).pipe(gulp.dest(buildPath));

    gulp.src(['./php-classes/**/*.*', '!./php-classes/**/tests/*.*'])
        .pipe(rename(function (path) {
            path.basename = path.basename.toLowerCase();
        }))
        .pipe(gulp.dest(buildPath + '/php-classes'));

    gulp.src('./templates/**/*').pipe(gulp.dest(buildPath + '/templates'));

    gulp.src('./bower_components/kozutils/dist/KozUtils-AIO.js').pipe(gulp.dest(buildPath + '/bower_components/kozutils/dist'));
    gulp.src('./bower_components/kozmul/dist/KozMUL.js').pipe(gulp.dest(buildPath + '/bower_components/kozmul/dist'));

    gulp.src('./vendor/twbs/bootstrap/dist/css/bootstrap.min.css').pipe(gulp.dest(buildPath + '/vendor/twbs/bootstrap/dist/css'));
    gulp.src('./vendor/twbs/bootstrap/dist/js/bootstrap.min.js').pipe(gulp.dest(buildPath + '/vendor/twbs/bootstrap/dist/js'));
    gulp.src('./vendor/components/jquery/jquery.min.js').pipe(gulp.dest(buildPath + '/vendor/components/jquery'));
    gulp.src('./vendor/autoload.php').pipe(gulp.dest(buildPath + '/vendor'));
    gulp.src('./vendor/composer/*').pipe(gulp.dest(buildPath + '/vendor/composer'));
});
