<?php
namespace ru\kozalo\simplewebsiteframework\controllers;

use ru\kozalo\simplewebsiteframework\models\VersionInfoModel;

class TestController implements IController
{
    /**
     * GetPathPatterns
     *
     * Returns an array of some regular expressions, which ControllerManager uses to compare with the URL.
     *
     * @param void
     * @return array
     */
    public function GetPathPatterns()
    {
        return [
            'version',
            'author',
            'info',
            'about',
            '/'
        ];
    }

    /**
     * ProcessRequest
     *
     * ControllerManager invokes this method if one of patterns matches to the URL.
     * It must process the request and outputs either a template, or a JSON object.
     *
     * @param string $matchedPattern
     * @param string $urlPath
     */
    public function ProcessRequest($matchedPattern, $urlPath)
    {
        // Here you should list all sections you're going to use.
        $SWF['sections'] = [
            'version_section'
        ];

        // Here we can list all additional styles and scripts we need for the page.
        $SWF['css'] = [];
        $SWF['js'] = [];

        // Of course, you may pass to templates any parameters you want.
        if ($matchedPattern == 'about' || $matchedPattern == 'author')
            $SWF['author'] = VersionInfoModel::GetAuthor();
        if ($matchedPattern == 'version' || $matchedPattern == "info")
            $SWF['version'] = VersionInfoModel::GetVersion();

        // At the end, include the base template, which starts the process of recursive includes.
        include('templates/basement');
    }
}