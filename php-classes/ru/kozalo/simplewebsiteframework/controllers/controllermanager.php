<?php
namespace ru\kozalo\simplewebsiteframework\controllers;

use ReflectionClass;

/**
 * Class ControllerManager
 * Singleton
 *
 * In fact, this is the main class of the whole website.
 * Used by index.php to start the system of controllers.
 * Matches the current URL and patterns pulled from GetPathPatterns() methods, and pass the control flow to the matched controller.
 *
 * @package ru\kozalo\congrats\controllers
 * @author Kozalo <kozalo@yandex.ru>
 * @copyright Kozalo.Ru, 2016
 * @uses ReflectionClass
 */
class ControllerManager
{
    /**
     * instance
     * @var ControllerManager
     */
    private static $instance;


    /**
     * namespace
     *
     * By default, this variable is set to the namespace of this class.
     * If you want to place your controllers in another package, determine a new namespace when calling the Init() function.
     *
     * @var string
     */
    private $namespace;


    /**
     * controllers
     *
     * An array of controllers. See Init() and AddController() for more information.
     *
     * @var array
     * @see Init(), AddController()
     */
    private $controllers;


    /**
     * debugMode
     *
     * See IsDebugMode() and DebugMode() for more information.
     *
     * @var bool
     * @see IsDebugMode(), DebugMode()
     */
    private static $debugMode = false;


    /**
     * maintenanceMode
     *
     * See MaintenanceMode() for more information.
     *
     * @var bool
     * @see MaintenanceMode()
     */
    private static $maintenanceMode = false;


    /**
     * IsDebugMode
     *
     * Controllers are supposed to call this static method to get known if debugMode is true.
     * They should use that information to variate their output.
     *
     * @param void
     * @return bool
     */
    public static function IsDebugMode()
    {
        return self::$debugMode;
    }


    /**
     * DebugMode
     *
     * Enables the debug mode.
     *
     * @params void
     * @return void
     */
    public static function DebugMode()
    {
        error_reporting(E_ALL);
        self::$debugMode = true;
    }


    /**
     * MaintenanceMode
     *
     * Enables a maintenance mode.
     *
     * @params void
     * @return void
     */
    public static function MaintenanceMode()
    {
        self::$maintenanceMode = true;
    }


    /**
     * Init
     *
     * Loads controllers from ListOfControllers.cfg and creates an instance of the singleton.
     *
     * @param string|null $namespace Used to determine a namespace for controllers. By default, it's the namespace of this class.
     * @return ControllerManager
     * @uses AddController()
     */
    public static function Init($namespace = null)
    {
        if (empty(self::$instance) || !(self::$instance instanceof self))
            self::$instance = new self();

        if (isset($namespace)) {
            self::$instance->namespace = $namespace;
        } else {
            $class = new ReflectionClass(self::$instance);
            self::$instance->namespace = $class->getNamespaceName();
        }

        self::$instance->controllers = [];
        $controllerNames = include('list_of_controllers.cfg');

        foreach($controllerNames as $name)
            self::$instance->AddController($name);

        return self::$instance;
    }


    /**
     * AddController
     *
     * Gets the name of a controller class on input, checks if this class exists,
     * returns true, if the controller was successfully created and appended to the $controllers array, or false otherwise.
     *
     * @param $className
     * @return bool
     */
    public function AddController($className)
    {
        $className = $this->namespace . '\\' . $className;

        if (class_exists($className))
        {
            $controller = new $className();
            if ($controller instanceof IController)
            {
                $this->controllers[] = new $className();
                return true;
            }
        }

        return false;
    }


    /**
     * RemoveController
     *
     * Search the $controllers array for the given controller.
     * Returns true, if the controller was successfully removed from the array, or false otherwise.
     *
     * @param string $className
     * @return bool
     */
    public function RemoveController($className)
    {
        $className = $this->namespace . '\\' . $className;

        for ($i = 0; $i < $this->GetCountOfController(); $i++)
        {
            $controller = $this->controllers[$i];
            if ($className == get_class($controller))
            {
                unset($controller);
                $this->controllers = array_values($this->controllers);
                return true;
            }
        }

        return false;
    }


    /**
     * InvokeAppropriateController
     *
     * Matches the current URL and patterns pulled from GetPathPatterns() methods, and pass the control flow to the matched controller.
     * It uses the hash symbol (#) as a delimiter. So, if you use this symbol in your expression, you must escape it.
     *
     * @param void
     * @return void
     */
    public function InvokeAppropriateController()
    {
        if (self::$maintenanceMode)
            header("Location: /maintenance.html");

        if (isset($_GET['route']))
            $route = $_GET['route'];
        else
            $route = '/';

        foreach($this->controllers as $controller)
        {
            // $pattern = '+^' . implode('|', $controller->GetPathPatterns()) . '$+';
            // Nested loop instead of the clause above lets us pass the matched pattern into a ProcessRequest() method.
            foreach($controller->GetPathPatterns() as $pattern)
            {
                if (preg_match('#^'.$pattern.'$#', $route)) {
                    $controller->ProcessRequest($pattern, $route);
                    return;
                }
            }
        }

        header("Location: /404.html");
    }


    /**
     * GetAmountOfController
     *
     * You can check the number of loaded controllers after the Init() method finishes.
     *
     * @param void
     * @return int
     */
    public function GetCountOfController()
    {
        return count($this->controllers);
    }
}