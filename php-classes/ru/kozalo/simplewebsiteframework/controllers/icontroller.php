<?php
namespace ru\kozalo\simplewebsiteframework\controllers;

/**
 * Interface IController
 *
 * Used for the glory of polymorphism ;)
 *
 * @author Kozalo <kozalo@yandex.ru>
 * @copyright Kozalo.Ru, 2016
 * @package ru\kozalo\congrats\controllers
 */
interface IController {
    /**
     * GetPathPatterns
     *
     * Returns an array of some regular expressions, which ControllerManager uses to compare with the URL.
     *
     * @param void
     * @return array
     */
    public function GetPathPatterns();

    /**
     * ProcessRequest
     *
     * ControllerManager invokes this method if one of patterns matches to the URL.
     * It must process the request and outputs either a template, or a JSON object.
     *
     * @param string $matchedPattern
     * @param string $urlPath
     */
    public function ProcessRequest($matchedPattern, $urlPath);
}