<?php
namespace ru\kozalo\simplewebsiteframework\models;


class VersionInfoModel
{
    public static function GetAuthor()
    {
        return [
            'name' => 'Leonid Kozarin',
            'email' => 'kozalo@yandex.ru',
            'website' => 'http://kozalo.ru'
        ];
    }

    public static function GetVersion()
    {
        return 'v1.0.0 (built on 16 Nov. 2017)';
    }
}