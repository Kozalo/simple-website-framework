"use strict";

/**
 * @class jQuery
 * @classdesc I had to define this class to make JSDoc include my function in the documentation.
 */

jQuery.fn.extend({

    /**
     * Search the given form for any validation errors and marks all wrong inputs with the _has-error_ class.
     * If it couldn't find any errors, returns true. Otherwise, returns false.
     *
     * @author Kozalo <kozalo@yandex.ru>
     * @copyright Kozalo.Ru, 2016
     * @returns {Boolean}
     * @memberof jQuery
     */
    runFormValidator: function () {
        var everythingFine = true;
        jQuery(this).find(".has-error").removeClass("has-error");

        jQuery(this).find("[required]").each(function () {
            var val = jQuery(this).val();

            if (val instanceof Array) {
                if (val.length === 0)
                    Error(this);
            }
            else {
                if (jQuery(this).val() === null || jQuery(this).val().trim() === "")
                    Error(this);
            }
        });

        jQuery(this).find("[minlength]").each(function () {
            if (jQuery(this).val().length < jQuery(this).attr("minlength"))
                Error(this);
        });

        jQuery(this).find("[maxlength]").each(function () {
            if (jQuery(this).val().length > jQuery(this).attr("maxlength"))
                Error(this);
        });

        jQuery(this).find("[type=email]").each(function () {
            if (!jQuery(this).val().match(/^([\wА-Яа-я.\-]+@[\wА-Яа-я.\-]+\.[a-zA-ZрфРФ]{2,})?$/))
                Error(this);
        });

        jQuery(this).find("[type=number]").each(function () {
            if (!jQuery.isNumeric(jQuery(this).val()))
                Error(this);
        });

        jQuery(this).find("[min]").each(function () {
            if (jQuery(this).val() < jQuery(this).attr("min"))
                Error(this);
        });

        jQuery(this).find("[max]").each(function () {
            if (jQuery(this).val() > jQuery(this).attr("max"))
                Error(this);
        });

        jQuery(this).find("[pattern]").each(function () {
            if (!jQuery(this).val().match(new RegExp(jQuery(this).attr("pattern"))))
                Error(this);
        });

        jQuery(this).find("[data-must-be-same]").each(function () {
            var mustBeSame = jQuery(this).closest("form").find('#' + jQuery(this).data("must-be-same"));
            if (jQuery(this).val() !== mustBeSame.val())
                Error(this);
        });

        return everythingFine;


        function Error(elem) {
            jQuery(elem).closest(".form-group").addClass("has-error");
            everythingFine = false;
        }
    }

});