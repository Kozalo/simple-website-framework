"use strict";

$(function(){
    $(".has-tooltip").tooltip();

    $(".section-text").each(function(){
        $(this).html(
            ProcessText($(this).text())
        );
    });

    // Write your code here
});


/**
 * Shows the error modal window with the given text to the user
 * @param {String} text
 * @author Kozalo <kozalo@yandex.ru>
 * @copyright Kozalo.Ru, 2016
 */
function ShowErrorModal(text)
{
    var modalWindow = $("#modalWindow");
    modalWindow.find(".modal-title").text("Ошибка!");
    modalWindow.find(".modal-body > p").html(text);
    modalWindow.modal();
}


/**
 * Shows the success modal window with the given text to the user
 * @param {String} text
 * @author Kozalo <kozalo@yandex.ru>
 * @copyright Kozalo.Ru, 2016
 */
function ShowSuccessModal(text)
{
    var modalWindow = $("#modalWindow");
    modalWindow.find(".modal-title").text("Успешно!");
    modalWindow.find(".modal-body > p").html(text);
    modalWindow.modal();
}


/**
 * Wrapper over my implementation of KozMUL. Use this method to be able to change compiler parameters globally.
 *
 * @param {String} text
 * @returns {String}
 * @author Kozalo <kozalo@yandex.ru>
 * @copyright Kozalo.Ru, 2016
 */
function ProcessText(text)
{
    // Change parameters of the constructor if you need to.
    var compiler = new KozMUL({
        transformHTMLEntities: true,
        transformBasicStyleTags: true,
        transformTypographicalCharacters: true,
        transformSubscriptAndSuperscriptTags: true,
        transformParagraphs: true,
        transformLists: true,
        transformQuotations: true,
        transformHorizontalLines: false,
        transformHeadings: false,
        transformLinks: true,
        transformImages: false,
        transformTables: false,
        transformCodeTags: false,
        transformSubstitutions: false
    }, {
        targetBlank: false,
        tableClasses: ['table', 'table-bordered'],
        listsWithIndentations: true,
        mostImportantHeading: 3
    });

    return compiler.toHTML(text);
}


/**
 * Wrapper over jQuery.post() to make sending requests even easier.
 * The server-side must return a valid JSON. One of its fields must be either true, or false, and represents the success of the request. You can name this field as you wish. But its name has to be passed into the function as a _statusField_.
 * Unlike jQuery's, this function takes the processing of fails on itself, so you have to pass into it only a text message to show to the user.
 * If you pass a reference to the clicked button, its state will be changed via $().button() automatically.
 *
 * @param {String} url
 * @param {Object} data
 * @param {String} statusField
 * @param {successCallback} successCallback
 * @param {String} failMessage
 * @param {HTMLElement=} clickedButton
 * @author Kozalo <kozalo@yandex.ru>
 * @copyright Kozalo.Ru, 2016
 */
function PerformPostRequest(url, data, statusField, successCallback, failMessage, clickedButton)
{
    if (clickedButton !== undefined)
        $(clickedButton).button("loading");

    $.post(url, data).done(function(response) {
        if (clickedButton !== undefined)
            $(clickedButton).button("reset");

        var result;

        try {
            result = JSON.parse(response);
        }
        catch (e) {
            result = {};
            result[statusField] = false;
            result.reason = "Получен некорректный JSON.";
            console.log(response);
        }

        if (result[statusField])
            successCallback(result);
        else
            ShowErrorModal(failMessage + ': ' + result.reason);

    }).fail(function(jqxhr, status) {
        if (clickedButton !== undefined)
            $(clickedButton).button("reset");

        ShowErrorModal("Не удалось выполнить запрос к серверу: " + status);
    });
}

/**
 * This callback is displayed as a global success callback.
 * @callback successCallback
 * @param {Object} data
 */
